﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using odiProject.Models;
using odiProject.ViewModels;

namespace odiProject.Controllers
{
    public class CustomerController : Controller
    {
        CustomerModel model;

        public CustomerController() {
            model = new CustomerModel();
        }

        // GET: Customer
        public ActionResult Index()
        {
            using (DbModels dbModel = new DbModels())
            {
                return View(dbModel.customers.ToList());
            }
            //return View();
        }

        // GET: Customer/Details/5
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return RedirectToAction("Index","Customer");
            }

            using(DbModels dbModels = new DbModels())
            {
                return View(dbModels.customers.Where(x => x.customer_id == id).FirstOrDefault());
            }
            //return View();
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(CustomerViewModels newCustomer)
        {
            try
            {
                model.Create(newCustomer);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(ex);
            }
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return RedirectToAction("Index", "Customer");
            }

            using (DbModels dbModels = new DbModels())
            {
                return View(dbModels.customers.Where(x => x.customer_id == id).FirstOrDefault());
            }
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, customer customers)
        {
            try
            {
                using(DbModels dbModel = new DbModels())
                {
                    dbModel.Entry(customers).State = EntityState.Modified;
                    dbModel.SaveChanges();
                }
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(ex);
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
            {
                return RedirectToAction("Index", "Customer");
            }

            using (DbModels dbModels = new DbModels())
            {
                return View(dbModels.customers.Where(x => x.customer_id == id).FirstOrDefault());
            }
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                using (DbModels dbModel = new DbModels())
                {
                    customer customer = dbModel.customers.Where(x => x.customer_id == id).FirstOrDefault();
                    dbModel.customers.Remove(customer);
                    dbModel.SaveChanges();
                }
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
