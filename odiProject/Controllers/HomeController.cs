﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using odiProject.Models;
using odiProject.ViewModels;

namespace odiProject.Controllers
{
    public class HomeController : Controller
    {
        RegisterModel model;

        public HomeController()
        {
            model = new RegisterModel();
        }

        public ActionResult Index()
        {
            ViewBag.Error = "";
            //return RedirectToAction("Index", "Customer");
            return View();
        }

        [HttpPost]
        public ActionResult Index(RegisterViewModel register)
        {
            DbModels db = new DbModels();

            var LoggedIn = db.Registers.Where(x => x.email_add == register.EmailAdd && x.password == register.Password).FirstOrDefault();

            if(LoggedIn != null)
            {
                Session["username"] = register.EmailAdd;
                Session["password"] = register.Password;
            }

            ViewBag.Error = Session["username"];
            
               //ViewBag.Error = model.ValidateLogin(Password, EmailAdd);
            
            
            return RedirectToAction("Index", "Customer");
            //return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Create()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(customer newCustomers)
        {
            try
            {
                using (DbModels dbModels = new DbModels())
                {
                    dbModels.customers.Add(newCustomers);
                    dbModels.SaveChanges();
                }
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(ex);
            }
        }

        [HttpPost]
        public ActionResult Validate()
        {
            ViewBag.Error = "Invalid Login Please Try Again!!!";

            return RedirectToAction("Index", "Home");
        }
    }
}