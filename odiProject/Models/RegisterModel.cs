﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using odiProject.ViewModels;

namespace odiProject.Models
{
    public class RegisterModel
    {
        public Register DefineName(RegisterViewModel register)
        {
            Register newregister = new Register();
            newregister.first_name = register.FirstName;
            newregister.last_name = register.LastName;
            newregister.account_id = register.AccountID;
            newregister.address = register.Address;
            newregister.city = register.City;
            newregister.state = register.State;
            newregister.zip_code = register.ZipCode;
            newregister.email_add = register.EmailAdd;

            return newregister;
        }

        public void Create(RegisterViewModel register)
        {
            var newregisters = DefineName(register);


            using (DbModels dbModels = new DbModels())
            {
                dbModels.Registers.Add(newregisters);
                dbModels.SaveChanges();
            }
        }

        /*public bool ValidateLogin(String pass, String email)
        {
            Register register = new Register();
            DbModels models = new DbModels();
            //var k = models.Registers.Where(x => x.email_add == email).FirstOrDefault();

            var query = (from p in models.Registers
                         where p.email_add == email
                         orderby p.first_name
                         select p).ToList();

            bool k = false;
            var i = "";

            
            
            return k;
            
        }*/
    }
}